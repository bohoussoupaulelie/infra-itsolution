
Instructions en ligne de commande

Vous pouvez également téléverser des fichiers existants depuis votre ordinateur en suivant les instructions ci-dessous.
Configuration globale de Git

git config --global user.name "Bohoussou elie paul david"
git config --global user.email "bohoussoupaulelie@gmail.com"

Créer un nouveau dépôt

git clone https://gitlab.com/bohoussoupaulelie/infra-itsolution.git
cd infra-itsolution
git switch -c main
touch README.md
git add README.md
git commit -m "add README"
git push -u origin main

Pousser un dossier existant

cd existing_folder
git init --initial-branch=main
git remote add origin https://gitlab.com/bohoussoupaulelie/infra-itsolution.git
git add .
git commit -m "Initial commit"
git push -u origin main

Pousser un dépôt Git existant

cd existing_repo
git remote rename origin old-origin
git remote add origin https://gitlab.com/bohoussoupaulelie/infra-itsolution.git
git push -u origin --all
git push -u origin --tags

